const hoursInput = document.getElementById('hours');
hoursInput.addEventListener('input', hoursChange);

const minutesInput = document.getElementById('minutes');
minutesInput.addEventListener('input', minutesChange);

const secondsInput = document.getElementById('seconds');
secondsInput.addEventListener('input', secondsChange);
const button = document.getElementById('button');

let timeleft = 0;
let message = { title: "Time's up!", body: "The timer has ended counting down." };
let isCountingDown = false;
let timer;

function start() {
    if (!isCountingDown) {
        countTime();
        if (timeleft <= 0) {
            return;
        }
        isCountingDown = true;
        disableFields();
        countDown();
        button.innerHTML = "STOP";
    } else {
        isCountingDown = false;
        enableFields();
        clearInterval(timer);
        timeleft = 0;
        repaintFields();
        button.innerHTML = "START";
    }
}

function countDown() {
    repaintFields();
    timer = setInterval(function () {
        if (timeleft > 0) {
            timeleft--;
            repaintFields();
        } else {
            clearTimeout(timer);
            countEnded();
        }
    }, 1000);
}

function countEnded() {
    isCountingDown = false;
    enableFields();
    sendNotification();
    button.innerHTML = "START";
}

function repaintFields() {
    let seconds = timeleft;

    let hours = Math.floor(seconds / 3600);
    seconds -= hours * 3600;
    hoursInput.value = hours > 0 ? hours : "00";

    let minutes = Math.floor(seconds / 60)
    minutesInput.value = minutes > 0 ? minutes : "00";
    seconds -= minutes * 60;

    secondsInput.value = seconds > 0 ? seconds : "00";
}

function countTime() {
    timeleft += +secondsInput.value;
    timeleft += +minutesInput.value * 60;
    timeleft += +hoursInput.value * 3600;
}

function countEnded() {
    sendNotification();
}

function disableFields() {
    hoursInput.disabled = true;
    minutesInput.disabled = true;
    secondsInput.disabled = true;
}

function enableFields() {
    hoursInput.disabled = false;
    minutesInput.disabled = false;
    secondsInput.disabled = false;
}


function sendNotification() {
    new Notification(message.title, { body: message.body });
}

function hoursChange(event) {
    if (event.srcElement.value < 0) {
        event.srcElement.value = 0;
        event.stopImmediatePropagation();
        event.cancelBubble();
    }
}

function minutesChange(event) {
    if (event.srcElement.value < 0) {
        event.srcElement.value = 0;
        event.stopImmediatePropagation();
        event.cancelBubble();
    } else if (event.srcElement.value >= 60) {
        ++hoursInput.value;
        minutesInput.value = 0;
        event.stopImmediatePropagation();
        event.cancelBubble();
    }
}

function secondsChange(event) {
    if (event.srcElement.value < 0) {
        event.srcElement.value = 0;
        event.stopImmediatePropagation();
        event.cancelBubble();
    } else if (event.srcElement.value >= 60) {
        ++minutesInput.value;
        secondsInput.value = 0;
        event.stopImmediatePropagation();
        event.cancelBubble();
    }
}
