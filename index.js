
'use strict';
const electron = require('electron')
// Module to control application life.
const app = electron.app
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow


function createWindow() {
  // Create the browser window.
  let win = new BrowserWindow({ width: 230, height: 115, resizable: false })

  // and load the index.html of the app.
  win.loadFile('index.html')
}

app.on('ready', createWindow)


